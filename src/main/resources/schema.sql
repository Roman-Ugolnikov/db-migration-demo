
--
-- Table structure for table `systemuser`
--

DROP TABLE IF EXISTS `article`;
DROP TABLE IF EXISTS `systemuser`;

CREATE TABLE `systemuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `login` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `content` longtext,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_author_idx` (`author_id`),
  CONSTRAINT `fk_author` FOREIGN KEY (`author_id`) REFERENCES `systemuser` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `systemuser`
--

LOCK TABLES `systemuser` WRITE;
/*!40000 ALTER TABLE `systemuser` DISABLE KEYS */;
INSERT INTO `systemuser` VALUES (4,'Roman','roman'),(5,'Pavel','pavel'),(6,'Ivan Dorn','ivan');
/*!40000 ALTER TABLE `systemuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (0,'Ancient Rome','Ancient Rome was an Italic civilization that began on the Italian Peninsula as early as the 8th century BC. Located along the Mediterranean Sea and centered on the city of Rome, it expanded to become one of the largest empires in the ancient world[1] with',4),(1,'Ancient Greece ','Ancient Greece was a civilization belonging to a period of Greek history that lasted from the Archaic period of the 8th to 6th centuries BC[citation needed] to the end of antiquity (c. 600 AD). Immediately following this period was the beginning of the Ea',5),(2,'Ancient Egypt ','Ancient Egypt was a civilization of ancient Northeastern Africa, concentrated along the lower reaches of the Nile River in what is now the modern country of Egypt. It is one of six civilizations to arise independently. Egyptian civilization followed prehistoric Egypt and coalesced around 3150 BC (according to conventional Egyptian chronology)[1] with the political unification of Upper and Lower Egypt under the first pharaoh Narmer (commonly referred to as Menes)',5);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;


