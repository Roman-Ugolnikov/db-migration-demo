package liquibase.controller;

import liquibase.domain.ArticlePresentation;
import liquibase.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author r.uholnikov
 */
@Controller
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @ResponseBody
    @RequestMapping(value = "/articles/{id}")
    public ArticlePresentation getOne(@PathVariable long id) {
        return articleService.findOnePresentation(id);
    }

    @ResponseBody
    @RequestMapping(value = "/articles")
    public Iterable<ArticlePresentation> getAll() {
        return articleService.findAllPresentation();
    }
}
