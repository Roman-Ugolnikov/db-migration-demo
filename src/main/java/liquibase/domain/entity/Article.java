package liquibase.domain.entity;


import java.util.Collection;
import javax.persistence.*;

/**
 * @author r.uholnikov
 */
@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String title;

    @ManyToOne(targetEntity = SystemUser.class, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private SystemUser author;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SystemUser getAuthor() {
        return author;
    }

    public void setAuthor(SystemUser author) {
        this.author = author;
    }

}
