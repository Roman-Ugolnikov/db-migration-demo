package liquibase.domain.entity;


import javax.persistence.*;

/**
 * @author r.uholnikov
 */
@Entity
@Table(name = "article_content")
public class ArticleContent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String language;

    private String content;

    @ManyToOne(targetEntity = Article.class, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Article article;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
