package liquibase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import liquibase.domain.ArticlePresentation;
import liquibase.domain.entity.Article;
import liquibase.domain.entity.ArticleContent;
import liquibase.repository.ArticleContentRepository;
import liquibase.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;


/**
 * @author r.uholnikov
 */
@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleContentRepository articleContentRepository;

    public Article findOne(Long id) {
        return articleRepository.findOne(id);
    }

    public Iterable<Article> findAll() {
        return articleRepository.findAll();
    }

    public ArticlePresentation findOnePresentation(Long id) {
        return getPresentation(findOne(id));
    }

    public Iterable<ArticlePresentation> findAllPresentation() {
        Iterable<Article> entities = findAll();
        List<ArticlePresentation> articlePresentationList = new ArrayList<>();
        entities.iterator().forEachRemaining(article -> articlePresentationList.add(getPresentation(article)));
        return articlePresentationList;
    }


    /**
     * Build {@link ArticlePresentation} based on reseived {@link Article} instance.
     * @param article to be presented to enduser
     * @return representation object for the article
     */
    private ArticlePresentation getPresentation(Article article) {
        ArticlePresentation result = null;
        if(article != null){
            result = new ArticlePresentation(article.getTitle(),
                                             getLocalizedContent(article),
                                             article.getAuthor().getName());
        }
        return result;
    }

    private String getLocalizedContent(Article article) {
        String language = LocaleContextHolder.getLocale().getLanguage();
        Optional<ArticleContent> first = articleContentRepository.findByLanguageLikeAndArticle(language, article)
                .stream().findFirst();
        return first.isPresent()? first.get().getContent(): "";
    }


}
