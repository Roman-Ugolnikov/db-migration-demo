package liquibase.repository;

import java.util.Collection;
import liquibase.domain.entity.Article;
import liquibase.domain.entity.ArticleContent;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleContentRepository extends PagingAndSortingRepository<ArticleContent, Long> {

    Collection<ArticleContent> findByLanguageLikeAndArticle(String locale, Article article);
}
