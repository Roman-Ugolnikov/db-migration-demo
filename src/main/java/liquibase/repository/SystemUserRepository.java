package liquibase.repository;

import liquibase.domain.entity.SystemUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface SystemUserRepository extends PagingAndSortingRepository<SystemUser, Long> {

}
