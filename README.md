# README #

This is Spring Boot a project to demonstrate how automatic db migration can be applied.  

### Versions and branches ###
**V1.0** Is version of application without liquidate or flyway
**master head** is version of application with updated java code, but without db migration applied (application normally will not start)
**migration-flyway head** migration applied using flyway
**migration-liquibase head** migration applied using liquibase.

### Run ###
This project by default is configured to use Mysql db with user **rot** and password. **0000**. 

